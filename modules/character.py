# character.py

import random

# 도덕질문
# 개인사항 질문
questions = [
	"당신의 {name}은(는) 아픈 사람이 있을때 자신을 희생해서까지 도우려 합니까?",
	"당신의 {name}은(는) 때론 개인의 자유보다 더 중요한 것이 있다고 판단합니까?",
	"당신의 {name}은(는) 엄격한 정의 위엔 그 무엇도 없다고 생각합니까?",

	"당신의 {name}은(는) 새로운 장소에 처음으로 도착하는것에 관심이 있습니까?",
	"당신의 {name}은(는) 자신의 몸을 가꾸는것을 좋아합니까?",
	"당신의 {name}은(는) 무엇인가를 만들어 내는것을 좋아합니까?",

	"당신의 {name}은(는) 1",
	"당신의 {name}은(는) 2",
	"당신의 {name}은(는) 3",
	"당신의 {name}은(는) 4",
	"당신의 {name}은(는) 5",
	"당신의 {name}은(는) 6",
	"당신의 {name}은(는) 7",
	"당신의 {name}은(는) 8",
	"당신의 {name}은(는) 9"
]

class Character:
	checkings = ["name", "species"]

	def __init__(self, name, species):
		self.name, self.species = name, species
		self.answers = []

	def addQuestion(self, answer):
		if answer["question"] not in [x["question"] for x in self.answers]:
			self.answers.append(answer)

	def __eq__(self, other):
		if type(other) is not type(self):
			return False
		
		suc = True
		for i in self.checkings:
			if other.__getattribute__(i) != self.__getattribute__(i):
				suc = False

		return suc

	def __str__(self):
		return "<character {0}'s {1}>".format(self.species, self.name)

def getQuestion(char):
	done = [x["question"] for x in char.answers]
	text = random.choice([x for x in questions if x not in done])
	struct = {
		"question":text,
		"answer":None
	}
	char.answers.append(struct)

	return struct

def getQuestions(char, num):
	return [getQuestion(char) for x in range(0, num)]


if __name__ == "__main__":
	c = Character("test", "")
	getQuestion(c)