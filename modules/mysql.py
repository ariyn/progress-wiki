# mysql.py

import pymysql
import character

mySqlUrl = "webdatabase.camduwlmzhrm.ap-northeast-1.rds.amazonaws.com"

class MySql:
	connection = {}
	speciesData = None
	def connect(self, name, url = mySqlUrl):
		suc = False

		if name not in self.connection:
			con = pymysql.connect(url, "root", "pw4aws.amazon.com", charset='utf8')

			self.connection[name] = {
				"con": con,
				"cur": con.cursor(pymysql.cursors.DictCursor)
			}

			self.con, self.cur = self.connection[name]["con"], self.connection[name]["cur"]
			suc = self.selectDB(name)
		else:
			self.con, self.cur = self.connection[name]["con"], self.connection[name]["cur"]
			return self.con.ping()

	def selectDB(self, dbName):
		num = self.cur.execute("SHOW DATABASES;")
		if num:
			datas = self.cur.fetchall()
			if dbName in [x["Database"] for x in datas if "Database" in x]:
				self.cur.execute("USE "+dbName)
				return True

	# take these two methods to engine.py after engine.py
	def getCharacter(self, charName):
		query = "SELECT chars.name 'name', chars.pid 'pid', spec.name 'species', spec.health 'health' FROM py_characters chars INNER JOIN py_species spec on spec.pid = chars.species WHERE chars.name = '{charName}'".format(charName = charName)
		row = self.cur.execute(query)
		if row:
			data = self.cur.fetchone()
			return character.Character(data["name"], data["species"])

	def addCharacter(self, char):
		import sys
		species = [(x["pid"], x["name"]) for x in self.speciesData if x["name"] == char.species][0]
		query = "INSERT INTO py_characters VALUES(NULL, '{0}', {1})".format(char.name, species[0])

		row = self.cur.execute(query)
		if row:
			self.con.commit()
			return True

	def getSpecies(self, name=None, pid=None):
		query = "SELECT * FROM py_species "
		if name:
			query += "WHERE name = '{0}'".format(name)
		elif pid:
			query += "WHERE pid = '{0}'".format(pid)

		num = self.cur.execute(query)

		if num:
			return self.cur.fetchall()

