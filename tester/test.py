# test.py
import sys
import unittest
from importlib.machinery import SourceFileLoader

def printUTF(char):
	sys.stdout.buffer.write((str(char)+"\n").encode("utf-8"))

class TddInPythonExample(unittest.TestCase):
	def setUp(self):
		self.char = SourceFileLoader("character", "../modules/character.py").load_module()
		self.mysql_module = SourceFileLoader("Mysql", "../modules/mysql.py").load_module()
		self.mysql = self.mysql_module.MySql()
		self.mysql.connect("progress")

		data = self.mysql.getSpecies()
		self.mysql.speciesData = data

	def test_CharacterInit(self):
		char1, char2 = self.char.Character("ariyn", "human"), self.char.Character("pippin", "hobit")
		self.assertEqual(char1.name, "ariyn")
		self.assertEqual(char1.species, "human")
		self.assertEqual(char2.name, "pippin")
		self.assertEqual(char2.species, "hobit")

	def test_CharacterGetQuestion(self):
		char = self.char.Character("ariyn", "human")
		question = self.char.getQuestion(char)
		question["answer"] = True

	def test_CharacterGetQuestions(self):
		char = self.char.Character("ariyn", "human")
		
		questions = self.char.getQuestions(char, 5)

		for i in questions:
			i["answer"] = True

	def test_CharacterAddQuestion(self):
		char = self.char.Character("ariyn", "human")
		question = self.char.getQuestion(char)
		question["answer"] = True

		char.addQuestion(question)

	def test_ConnectMysql(self):
		suc = self.mysql.connect("progress")
		self.assertTrue(suc)

	def test_GetSpecies(self):
		self.assertIsNotNone(self.mysql.getSpecies())
		self.assertIsNotNone(self.mysql.getSpecies(name = "인간"))
		self.assertIsNotNone(self.mysql.getSpecies(pid = 1))

	def test_GetCharacter(self):
		char = self.char.Character("ariyn", "인간")

		character = self.mysql.getCharacter("ariyn")
		self.assertEqual(character, char)

	def test_CreateCharacter(self):
		char = self.char.Character("샤아", "인간")

		self.assertTrue(self.mysql.addCharacter(char))

if __name__ == '__main__':
	unittest.main()